import React, { Component } from 'react'
import PropTypes from 'prop-types'
import R from 'ramda'
import Table from './Table'

class TableContainer extends Component {
    constructor(props){
        super(props)
    }

    state = {
        query:"",
        sort: [],
        page: 1,
        
    }

    render(){
        return (
            <Table {...this.props} />
        )
    }
}

TableContainer.propTypes = {
    datas : PropTypes.arrayOf(PropTypes.object).isRequired(),
    headers : PropTypes.arrayOf(PropTypes.string).isRequired(),
    mappings : PropTypes.object
    
}