import React, { Component } from 'react'
import PropTypes from 'prop-types'
import R from 'ramda'

class Table extends Component {
    constructor(props){
        super(props)
    }

    render(){
        const {headers, datas}
        return (
            <table>
                <thead>
                    {R.forEach( 
                        ( head ) => (
                            <th>{head}</th>
                        ) 
                    )}
                </thead>
                <tbody>
                    {
                        
                    }
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        )
    }
}

export default Table