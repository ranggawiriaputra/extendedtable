import 'path'
import Table from '../src/Table'
import React from 'react'
import Enzyme, {render} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

test("Table is loaded correctly!", () => {
    expect(Table).toBeDefined()

    let renderedTable = render(<Table />)
    expect(renderedTable).toBeDefined()
})