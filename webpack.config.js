var path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'ExtendedTable.js',
      library: 'extended-table',
      libraryTarget: 'umd'
    },
    optimization : {
        splitChunks : {
            chunks : 'all'
        }
    }
}